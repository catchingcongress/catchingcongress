import * as React from 'react';
import { useLayoutEffect } from 'react';
import { BrowserRouter, Routes, Route, useLocation } from 'react-router-dom';
import { ThemeProvider, CssBaseline, useMediaQuery, createTheme } from '@mui/material';
import Navigation from './shared/NavigationBar';
import Home from './pages/home/HomePage';
import Congresspeople from './pages/congresspeople/CongresspeoplePage';
import Congressperson from './pages/congresspeople/CongresspersonInstance';
import Committees from './pages/committees/CommitteesPage';
import Committee from './pages/committees/CommitteeInstance';
import Industries from './pages/industries/IndustriesPage';
import Industry from './pages/industries/IndustryInstance';
import Visualizations from './pages/visualizations/VisualizationsPage';
import About from './pages/about/AboutPage';
import Search from './pages/search/SearchPage';

const ScrollToTop = ({ children }: any) => {
  const location = useLocation();
  useLayoutEffect(() => {
    window.scrollTo(0, 0);
  }, [location.pathname]);
  return children;
};

function App() {
  const prefersDarkMode = useMediaQuery('(prefers-color-scheme: dark)');

  const theme = React.useMemo(
    () =>
      createTheme({
        palette: {
          mode: prefersDarkMode ? 'dark' : 'light',
          primary: {
            main: '#3b3f41',
          },
        },
      }),
    [prefersDarkMode]
  );

  return (
    <div className="App" style={{ textAlign: 'center' }}>
      <BrowserRouter>
        <ScrollToTop>
          <ThemeProvider theme={theme}>
            <CssBaseline />
            <Navigation />
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/congresspeople" element={<Congresspeople />} />
              <Route path="/congresspeople/:id" element={<Congressperson />} />
              <Route path="/committees" element={<Committees />} />
              <Route path="/committees/:id" element={<Committee />} />
              <Route path="/industries" element={<Industries />} />
              <Route path="/industries/:id" element={<Industry />} />
              <Route path="/visualizations" element={<Visualizations />} />
              <Route path="/about" element={<About />} />
              <Route path="/search/:id" element={<Search />} />
            </Routes>
          </ThemeProvider>
        </ScrollToTop>
      </BrowserRouter>
    </div>
  );
}

export default App;
