import { Box, Typography } from '@mui/material';
import ModelTable from './CommitteesModelTable';

const Committees = () => {
  return (
    <Box>
      <Typography id="committeesPageTitle" gutterBottom variant="h3" component="div" pt={2}>
        Committees
      </Typography>
      <ModelTable />
    </Box>
  );
};

export default Committees;
