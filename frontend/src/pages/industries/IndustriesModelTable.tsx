import { Link } from 'react-router-dom';
import { useEffect, useState, MouseEventHandler } from 'react';
import { Highlight } from 'react-highlighter-ts';
import {
  Box,
  Grid,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  InputLabel,
  Select,
  MenuItem,
  FormControl,
  Radio,
  ListItemText,
  Tooltip,
  IconButton,
  CircularProgress,
} from '@mui/material';
import ClearAllIcon from '@mui/icons-material/ClearAll';
import SearchBar from '../../shared/SearchBar';
import Pagination from '../../shared/Pagination';
import { getIndustries } from '../../shared/APIRequests';
import { sectors, topChambers, topParties, sortOptions } from './IndustriesFilterInfo';
import { getHighlightSearch } from '../../shared/HighlightSearch';

function numberWithCommas(x: number) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}

const ModelTable = () => {
  const [industriesData, setIndustriesData] = useState<any[]>([]);
  const [page, setPage] = useState(0);
  const [count, setCount] = useState(0);
  const [sector, setSector] = useState('');
  const [topChamber, setTopChamber] = useState('');
  const [topParty, setTopParty] = useState('');
  const [sort, setSort] = useState('');
  const [sortParam, setSortParam] = useState('');
  const [order, setOrder] = useState('');
  const [query, setQuery] = useState('');
  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    getIndustries(page + 1, sector, topChamber, topParty, sortParam, order, query).then(
      (response: any) => {
        setIndustriesData(response['page']);
        setCount(response['count']);
        setLoaded(true);
      }
    );
  }, [page, sector, topChamber, topParty, sortParam, order, query]);

  const handleSectorChange = (event: any) => {
    setSector(event.target.value);
  };

  const handleTopChamberChange = (event: any) => {
    setTopChamber(event.target.value);
  };

  const handleTopPartyChange = (event: any) => {
    setTopParty(event.target.value);
  };

  const handleSortChange = (event: any) => {
    const value = event.target.value;
    setPage(0);
    setSort(value);
    if (value == 'name-asc') {
      setSortParam('name');
      setOrder('');
    } else if (value == 'name-desc') {
      setSortParam('name');
      setOrder('true');
    } else if (value == 'sector-asc') {
      setSortParam('sector');
      setOrder('');
    } else if (value == 'sector-desc') {
      setSortParam('sector');
      setOrder('true');
    } else if (value == 'amount-asc') {
      setSortParam('total_contributed');
      setOrder('');
    } else if (value == 'amount-desc') {
      setSortParam('total_contributed');
      setOrder('true');
    }
  };

  const handleQueryChange = (event: any) => {
    const value = event.target.value;
    setPage(0);
    if (value == '') {
      setQuery('');
    } else {
      setQuery(value);
    }
  };

  const handleClearAllClick: MouseEventHandler<HTMLButtonElement> = () => {
    setPage(0);
    setSector('');
    setTopChamber('');
    setTopParty('');
    setSort('');
    setSortParam('');
    setOrder('');
    setQuery('');
  };

  return (
    <Box>
      <Grid
        container
        spacing={0}
        pb={3}
        direction="column"
        alignItems="center"
        justifyContent="center"
      >
        <Grid id="searchBar" item xs={3}>
          <SearchBar query={query} onChange={handleQueryChange} />
        </Grid>

        <Grid item pl={2} xs={3}>
          <div
            style={{
              display: 'inline-flex',
              alignItems: 'center',
              flexWrap: 'wrap',
            }}
          >
            <FormControl id="sectorFilter" sx={{ m: 1, width: 180 }} variant="standard">
              <InputLabel>Sector</InputLabel>
              <Select
                data-testid="filter"
                defaultValue=""
                value={sector}
                onChange={handleSectorChange}
                renderValue={(selected) => selected}
              >
                {sectors.map((element, key) => (
                  <MenuItem key={key} value={element}>
                    <Radio checked={sector == element} />
                    <ListItemText primary={element} />
                  </MenuItem>
                ))}
              </Select>
            </FormControl>

            <FormControl sx={{ m: 1, width: 140 }} variant="standard">
              <InputLabel>Top chamber</InputLabel>
              <Select
                defaultValue=""
                value={topChamber}
                onChange={handleTopChamberChange}
                renderValue={(selected) => topChambers.find((i) => i.value === selected)?.name}
              >
                {topChambers.map((element, key) => (
                  <MenuItem key={key} value={element.value}>
                    <Radio checked={topChamber == element.value} />
                    <ListItemText primary={element.name} />
                  </MenuItem>
                ))}
              </Select>
            </FormControl>

            <FormControl sx={{ m: 1, width: 140 }} variant="standard">
              <InputLabel>Top party</InputLabel>
              <Select
                defaultValue=""
                value={topParty}
                onChange={handleTopPartyChange}
                renderValue={(selected) => topParties.find((i) => i.value === selected)?.name}
              >
                {topParties.map((element, key) => (
                  <MenuItem key={key} value={element.value}>
                    <Radio checked={topParty == element.value} />
                    <ListItemText primary={element.name} />
                  </MenuItem>
                ))}
              </Select>
            </FormControl>

            <FormControl id="sortMenu" sx={{ m: 1, width: 140 }} variant="standard">
              <InputLabel>Sort by</InputLabel>
              <Select
                data-testid="sort"
                defaultValue=""
                value={sort}
                onChange={handleSortChange}
                renderValue={(selected) => sortOptions.find((i) => i.value === selected)?.name}
              >
                {sortOptions.map((element, key) => (
                  <MenuItem key={key} value={element.value}>
                    {element.name}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>

            <div style={{ paddingTop: 12 }}>
              <Tooltip title="Clear all">
                <IconButton aria-label="clear-all" onClick={handleClearAllClick}>
                  <ClearAllIcon />
                </IconButton>
              </Tooltip>
            </div>
          </div>
        </Grid>
      </Grid>

      <Grid container justifyContent="center">
        {loaded ? (
          <TableContainer component={Paper} style={{ width: '80%' }}>
            <Table component="div" sx={{ minWidth: 650 }}>
              <TableHead component="div">
                <TableRow component="div">
                  <TableCell component="div">Name</TableCell>
                  <TableCell component="div" align="right">
                    Sector
                  </TableCell>
                  <TableCell component="div" align="right">
                    Top chamber
                  </TableCell>
                  <TableCell component="div" align="right">
                    Top party
                  </TableCell>
                  <TableCell component="div" align="right">
                    Amount contributed
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody component="div">
                {industriesData.map((industry) => (
                  <TableRow
                    hover
                    key={industry.ind_id}
                    component={Link}
                    to={`/industries/${industry.ind_id}`}
                    style={{ textDecoration: 'none' }}
                  >
                    <TableCell component="div" scope="row">
                      <Highlight search={getHighlightSearch(query)}>{industry.name}</Highlight>
                    </TableCell>
                    <TableCell component="div" align="right">
                      <Highlight search={getHighlightSearch(query)}>{industry.sector}</Highlight>
                    </TableCell>
                    <TableCell component="div" align="right">
                      {industry.chamber_most_contributed}
                    </TableCell>
                    <TableCell component="div" align="right">
                      {industry.party_most_contributed}
                    </TableCell>
                    <TableCell component="div" align="right">
                      ${numberWithCommas(industry?.total_contributed || 0)}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        ) : (
          <CircularProgress />
        )}
      </Grid>
      <Grid item xs={12}>
        <Grid container alignItems="center" justifyContent="center">
          <Pagination page={page} setPage={setPage} count={count} />
        </Grid>
      </Grid>
    </Box>
  );
};

export default ModelTable;
