const sectors = [
  'Agribusiness',
  'Communications/Electronics',
  'Construction',
  'Defense',
  'Energy & Natural Resources',
  'Finance, Insurance & Real Estate',
  'Health',
  'Lawyers & Lobbyists',
  'Transportation',
  'Misc Business',
  'Labor',
  'Ideological/Single-Issue',
  'Other',
];

const topChambers = [
  {
    name: 'House',
    value: 'house',
  },
  {
    name: 'Senate',
    value: 'senate',
  },
];

const topParties = [
  {
    name: 'Republican',
    value: 'R',
  },
  {
    name: 'Democrat',
    value: 'D',
  },
];

const sortOptions = [
  {
    name: 'Name (A-Z)',
    value: 'name-asc',
  },
  {
    name: 'Name (Z-A)',
    value: 'name-desc',
  },
  {
    name: 'Sector (A-Z)',
    value: 'sector-asc',
  },
  {
    name: 'Sector (Z-A)',
    value: 'sector-desc',
  },
  {
    name: 'Amount (Asc.)',
    value: 'amount-asc',
  },
  {
    name: 'Amount (Desc.)',
    value: 'amount-desc',
  },
];

export { sectors, topChambers, topParties, sortOptions };
