// @ts-nocheck
import { teamInfo } from './TeamInfo';

export const getGitlabInfo = async () => {
  let totalCommitCount = 0,
    totalIssueCount = 0,
    totalTestCount = 0;

  teamInfo.forEach((member) => {
    totalTestCount += member.tests;
    member.issues += 0;
    member.commits += 0;
  });

  let commitList = await fetch(
    'https://gitlab.com/api/v4/projects/33929223/repository/contributors'
  );
  commitList = await commitList.json();
  commitList.forEach((element) => {
    const { name, email, commits } = element;
    teamInfo.forEach((member) => {
      if (member.name === name || member.username === name || member.email === email) {
        member.commits += commits;
      }
    });
    totalCommitCount += commits;
  });

  const issuePaginationLength = 100;
  let page = 1;
  let issuePage = [];
  let issueList = [];
  do {
    issuePage = await fetch(
      `https://gitlab.com/api/v4/projects/33929223/issues?per_page=${issuePaginationLength}&page=${page++}`
    );
    issuePage = await issuePage.json();
    issueList = [...issueList, ...issuePage];
  } while (issuePage.length === 100);

  issueList.forEach((element) => {
    const { assignees } = element;
    assignees.forEach((a) => {
      const { name } = a;
      teamInfo.forEach((member) => {
        if (member.name === name || member.username === name) {
          member.issues += 1;
        }
      });
    });
    totalIssueCount += 1;
  });

  return {
    // Some commits were on the wrong Git account or not accounted for by the GitLab API (merge commits)
    totalCommits: totalCommitCount + 106,
    totalIssues: totalIssueCount,
    totalTests: totalTestCount,
    teamInfo: teamInfo,
  };
};
