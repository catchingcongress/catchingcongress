import CommitIcon from '@mui/icons-material/Commit';
import ListAltIcon from '@mui/icons-material/ListAlt';
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import { Grid, Typography, Card, CardContent, CardMedia, CardActionArea, Box } from '@mui/material';

const styles = {
  card: {
    height: '100%',
  },

  media: {
    height: '100%',
    width: '100%',
    objectFit: 'cover',
  },
} as const;

const MemberCard = (props: any) => {
  const { name, username, picture_path, role, bio, commits, issues, tests } = props;

  return (
    <Grid item xs={1} sm={4} md={3}>
      <a
        href={'https://gitlab.com/' + username}
        target="_blank"
        rel="noopener noreferrer"
        style={{ textDecoration: 'none' }}
      >
        <Card sx={{ maxWidth: 345 }} style={styles.card}>
          <CardActionArea style={{ outline: 'none' }}>
            <CardMedia component="img" image={picture_path} style={styles.media} />
            <CardContent>
              <Typography gutterBottom variant="h6" component="div">
                {name}
              </Typography>
              <Typography gutterBottom variant="body1" component="div">
                {role}
              </Typography>
              <Box sx={{ minHeight: '10vh' }}>
                <Typography variant="body2" color="text.secondary" component="div">
                  {bio}
                </Typography>
              </Box>
              <Typography variant="body1" color="text.secondary" p={2} component="div">
                <div
                  style={{
                    display: 'inline-flex',
                    alignItems: 'center',
                    flexWrap: 'wrap',
                  }}
                >
                  <CommitIcon style={{ position: 'relative', minWidth: '39px' }} />
                  {commits}
                </div>
                <div
                  style={{
                    display: 'inline-flex',
                    alignItems: 'center',
                    flexWrap: 'wrap',
                  }}
                >
                  <ListAltIcon style={{ position: 'relative', minWidth: '39px' }} />
                  {issues}
                </div>
                <div
                  style={{
                    display: 'inline-flex',
                    alignItems: 'center',
                    flexWrap: 'wrap',
                  }}
                >
                  <CheckCircleOutlineIcon style={{ position: 'relative', minWidth: '39px' }} />
                  {tests}
                </div>
              </Typography>
            </CardContent>
          </CardActionArea>
        </Card>
      </a>
    </Grid>
  );
};

export default MemberCard;
