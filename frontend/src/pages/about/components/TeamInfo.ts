import AnViImage from '../../../assets/images/team/an_vi.jpg';
import CarsonImage from '../../../assets/images/team/carson.jpg';
import DakshImage from '../../../assets/images/team/daksh.jpg';
import NathanImage from '../../../assets/images/team/nathan.jpg';
import NishaImage from '../../../assets/images/team/nisha.jpg';

// Some commits were on the wrong Git account or not accounted for by the GitLab API (merge commits)
const teamInfo = [
  {
    name: 'An Vi Nguyen',
    username: 'an-vi',
    email: 'avn040@gmail.com',
    picture_path: AnViImage,
    role: 'Backend',
    bio: "I'm a senior CS major at UT from Houston, TX. In my free time, I like to paddle board, work out, and hang out with friends!",
    commits: 2,
    issues: 0,
    tests: 5,
  },
  {
    name: 'Carson Saldanha',
    username: 'carsonsaldanha',
    email: 'carson.saldanha@gmail.com',
    picture_path: CarsonImage,
    role: 'Frontend | Phase 2 & 4 leader',
    bio: "I'm a junior CS major from Houston, TX double-minoring in Informatics and Business at UT. In my free time, I enjoy playing basketball!",
    commits: 61,
    issues: 0,
    tests: 49,
  },
  {
    name: 'Daksh Dua',
    username: 'dakshdua',
    picture_path: DakshImage,
    email: 'duadaksh@gmail.com',
    role: 'Backend | Phase 1 leader',
    bio: "I'm a senior CS major from central New Jersey with a certificate in Core Texts and Ideas at UT. In my free time, I love to read books.",
    commits: 42,
    issues: 0,
    tests: 0,
  },
  {
    name: 'Nathan Gates',
    username: 'nathangates117',
    picture_path: NathanImage,
    email: 'nathangates117@gmail.com',
    role: 'Backend | Phase 3 leader',
    bio: "I'm a senior CS and Math double major at UT from Denton, TX. I preside over and compete with UT Texas Ballroom!",
    commits: 3,
    issues: 0,
    tests: 6,
  },
  {
    name: 'Nisha Ramesh',
    username: 'nisharam7',
    picture_path: NishaImage,
    email: 'nisharamesh2001@yahoo.com',
    role: 'Backend',
    bio: "I'm a junior CS major at UT from Dallas, TX. In my free time, I love to hike, cook, and hang out with my friends!",
    commits: 0,
    issues: 0,
    tests: 4,
  },
];

export { teamInfo };
