import ReactLogo from '../../assets/logos/react.svg';
import MUILogo from '../../assets/logos/mui.svg';
import AWSLogo from '../../assets/logos/aws.svg';
import GitLabLogo from '../../assets/logos/gitlab.svg';
import PostmanLogo from '../../assets/logos/postman.svg';
import DockerLogo from '../../assets/logos/docker.svg';
import NamecheapLogo from '../../assets/logos/namecheap.svg';
import PrettierLogo from '../../assets/logos/prettier.svg';
import ESLintLogo from '../../assets/logos/eslint.svg';
import BlackLogo from '../../assets/logos/black.svg';
import MySQLLogo from '../../assets/logos/mysql.svg';
import FlaskLogo from '../../assets/logos/flask.svg';
import SQLAlchemyLogo from '../../assets/logos/sqlalchemy.svg';
import UnittestLogo from '../../assets/logos/unittest.svg';
import JestLogo from '../../assets/logos/jest.svg';
import SeleniumLogo from '../../assets/logos/selenium.svg';
import RechartsLogo from '../../assets/logos/recharts.svg';
import PlantUMLLogo from '../../assets/logos/plantuml.svg';
import DBDiagramLogo from '../../assets/logos/dbdiagram.svg';
import DiscordLogo from '../../assets/logos/discord.svg';
import {
  Container,
  Grid,
  Typography,
  Card,
  CardContent,
  CardMedia,
  CardActionArea,
  Box,
} from '@mui/material';

const styles = {
  card: {
    height: '96%',
  },

  media: {
    height: '100%',
    width: '100%',
    objectFit: 'cover',
  },
} as const;

const ToolsCards = () => {
  return (
    <Box>
      <Container>
        <Grid container spacing={{ xs: 2, md: 3 }} columns={{ xs: 2, sm: 8, md: 12 }} p={3}>
          <Grid item xs={1} sm={4} md={3}>
            <a
              href="https://reactjs.org/"
              target="_blank"
              rel="noopener noreferrer"
              style={{ textDecoration: 'none' }}
            >
              <Card sx={{ maxWidth: 345 }} style={styles.card}>
                <CardActionArea style={{ outline: 'none' }}>
                  <CardMedia component="img" image={`${ReactLogo}`} style={styles.media} />
                  <CardContent>
                    <Typography gutterBottom variant="h6" component="div">
                      React
                    </Typography>
                    <Box sx={{ minHeight: '10vh' }}>
                      <Typography variant="body2" color="text.secondary">
                        JavaScript library for front-end development
                      </Typography>
                    </Box>
                  </CardContent>
                </CardActionArea>
              </Card>
            </a>
          </Grid>

          <Grid item xs={1} sm={4} md={3}>
            <a
              href="https://mui.com/"
              target="_blank"
              rel="noopener noreferrer"
              style={{ textDecoration: 'none' }}
            >
              <Card sx={{ maxWidth: 345 }} style={styles.card}>
                <CardActionArea style={{ outline: 'none' }}>
                  <CardMedia component="img" image={`${MUILogo}`} style={styles.media} />
                  <CardContent>
                    <Typography gutterBottom variant="h6" component="div">
                      MUI
                    </Typography>
                    <Box sx={{ minHeight: '10vh' }}>
                      <Typography variant="body2" color="text.secondary">
                        React UI design library
                      </Typography>
                    </Box>
                  </CardContent>
                </CardActionArea>
              </Card>
            </a>
          </Grid>

          <Grid item xs={1} sm={4} md={3}>
            <a
              href="https://aws.amazon.com/"
              target="_blank"
              rel="noopener noreferrer"
              style={{ textDecoration: 'none' }}
            >
              <Card sx={{ maxWidth: 345 }} style={styles.card}>
                <CardActionArea style={{ outline: 'none' }}>
                  <CardMedia component="img" image={`${AWSLogo}`} style={styles.media} />
                  <CardContent>
                    <Typography gutterBottom variant="h6" component="div">
                      AWS
                    </Typography>
                    <Box sx={{ minHeight: '10vh' }}>
                      <Typography variant="body2" color="text.secondary">
                        Cloud hosting platform
                      </Typography>
                    </Box>
                  </CardContent>
                </CardActionArea>
              </Card>
            </a>
          </Grid>

          <Grid item xs={1} sm={4} md={3}>
            <a
              href="https://about.gitlab.com/"
              target="_blank"
              rel="noopener noreferrer"
              style={{ textDecoration: 'none' }}
            >
              <Card sx={{ maxWidth: 345 }} style={styles.card}>
                <CardActionArea style={{ outline: 'none' }}>
                  <CardMedia component="img" image={`${GitLabLogo}`} style={styles.media} />
                  <CardContent>
                    <Typography gutterBottom variant="h6" component="div">
                      GitLab
                    </Typography>
                    <Box sx={{ minHeight: '10vh' }}>
                      <Typography variant="body2" color="text.secondary">
                        Git repository and CI/CD platform
                      </Typography>
                    </Box>
                  </CardContent>
                </CardActionArea>
              </Card>
            </a>
          </Grid>

          <Grid item xs={1} sm={4} md={3}>
            <a
              href="https://www.postman.com/"
              target="_blank"
              rel="noopener noreferrer"
              style={{ textDecoration: 'none' }}
            >
              <Card sx={{ maxWidth: 345 }} style={styles.card}>
                <CardActionArea style={{ outline: 'none' }}>
                  <CardMedia component="img" image={`${PostmanLogo}`} style={styles.media} />
                  <CardContent>
                    <Typography gutterBottom variant="h6" component="div">
                      Postman
                    </Typography>
                    <Box sx={{ minHeight: '10vh' }}>
                      <Typography variant="body2" color="text.secondary">
                        Platform for designing and building APIs
                      </Typography>
                    </Box>
                  </CardContent>
                </CardActionArea>
              </Card>
            </a>
          </Grid>

          <Grid item xs={1} sm={4} md={3}>
            <a
              href="https://www.docker.com/"
              target="_blank"
              rel="noopener noreferrer"
              style={{ textDecoration: 'none' }}
            >
              <Card sx={{ maxWidth: 345 }} style={styles.card}>
                <CardActionArea style={{ outline: 'none' }}>
                  <CardMedia component="img" image={`${DockerLogo}`} style={styles.media} />
                  <CardContent>
                    <Typography gutterBottom variant="h6" component="div">
                      Docker
                    </Typography>
                    <Box sx={{ minHeight: '10vh' }}>
                      <Typography variant="body2" color="text.secondary">
                        Containers for consistent development environments
                      </Typography>
                    </Box>
                  </CardContent>
                </CardActionArea>
              </Card>
            </a>
          </Grid>

          <Grid item xs={1} sm={4} md={3}>
            <a
              href="https://www.namecheap.com/"
              target="_blank"
              rel="noopener noreferrer"
              style={{ textDecoration: 'none' }}
            >
              <Card sx={{ maxWidth: 345 }} style={styles.card}>
                <CardActionArea style={{ outline: 'none' }}>
                  <CardMedia component="img" image={`${NamecheapLogo}`} style={styles.media} />
                  <CardContent>
                    <Typography gutterBottom variant="h6" component="div">
                      Namecheap
                    </Typography>
                    <Box sx={{ minHeight: '10vh' }}>
                      <Typography variant="body2" color="text.secondary">
                        Domain name registrar
                      </Typography>
                    </Box>
                  </CardContent>
                </CardActionArea>
              </Card>
            </a>
          </Grid>

          <Grid item xs={1} sm={4} md={3}>
            <a
              href="https://prettier.io/"
              target="_blank"
              rel="noopener noreferrer"
              style={{ textDecoration: 'none' }}
            >
              <Card sx={{ maxWidth: 345 }} style={styles.card}>
                <CardActionArea style={{ outline: 'none' }}>
                  <CardMedia component="img" image={`${PrettierLogo}`} style={styles.media} />
                  <CardContent>
                    <Typography gutterBottom variant="h6" component="div">
                      Prettier
                    </Typography>
                    <Box sx={{ minHeight: '10vh' }}>
                      <Typography variant="body2" color="text.secondary">
                        Opinionated code formatter
                      </Typography>
                    </Box>
                  </CardContent>
                </CardActionArea>
              </Card>
            </a>
          </Grid>

          <Grid item xs={1} sm={4} md={3}>
            <a
              href="https://eslint.org/"
              target="_blank"
              rel="noopener noreferrer"
              style={{ textDecoration: 'none' }}
            >
              <Card sx={{ maxWidth: 345 }} style={styles.card}>
                <CardActionArea style={{ outline: 'none' }}>
                  <CardMedia component="img" image={`${ESLintLogo}`} style={styles.media} />
                  <CardContent>
                    <Typography gutterBottom variant="h6" component="div">
                      ESLint
                    </Typography>
                    <Box sx={{ minHeight: '10vh' }}>
                      <Typography variant="body2" color="text.secondary">
                        Pluggable JavaScript linter
                      </Typography>
                    </Box>
                  </CardContent>
                </CardActionArea>
              </Card>
            </a>
          </Grid>

          <Grid item xs={1} sm={4} md={3}>
            <a
              href="https://black.readthedocs.io/"
              target="_blank"
              rel="noopener noreferrer"
              style={{ textDecoration: 'none' }}
            >
              <Card sx={{ maxWidth: 345 }} style={styles.card}>
                <CardActionArea style={{ outline: 'none' }}>
                  <CardMedia component="img" image={`${BlackLogo}`} style={styles.media} />
                  <CardContent>
                    <Typography gutterBottom variant="h6" component="div">
                      Black
                    </Typography>
                    <Box sx={{ minHeight: '10vh' }}>
                      <Typography variant="body2" color="text.secondary">
                        Python code formatter
                      </Typography>
                    </Box>
                  </CardContent>
                </CardActionArea>
              </Card>
            </a>
          </Grid>

          <Grid item xs={1} sm={4} md={3}>
            <a
              href="https://www.mysql.com/"
              target="_blank"
              rel="noopener noreferrer"
              style={{ textDecoration: 'none' }}
            >
              <Card sx={{ maxWidth: 345 }} style={styles.card}>
                <CardActionArea style={{ outline: 'none' }}>
                  <CardMedia component="img" image={`${MySQLLogo}`} style={styles.media} />
                  <CardContent>
                    <Typography gutterBottom variant="h6" component="div">
                      MySQL
                    </Typography>
                    <Box sx={{ minHeight: '10vh' }}>
                      <Typography variant="body2" color="text.secondary">
                        Relational database management system
                      </Typography>
                    </Box>
                  </CardContent>
                </CardActionArea>
              </Card>
            </a>
          </Grid>

          <Grid item xs={1} sm={4} md={3}>
            <a
              href="https://flask.palletsprojects.com/"
              target="_blank"
              rel="noopener noreferrer"
              style={{ textDecoration: 'none' }}
            >
              <Card sx={{ maxWidth: 345 }} style={styles.card}>
                <CardActionArea style={{ outline: 'none' }}>
                  <CardMedia component="img" image={`${FlaskLogo}`} style={styles.media} />
                  <CardContent>
                    <Typography gutterBottom variant="h6" component="div">
                      Flask
                    </Typography>
                    <Box sx={{ minHeight: '10vh' }}>
                      <Typography variant="body2" color="text.secondary">
                        Micro web framework for API development
                      </Typography>
                    </Box>
                  </CardContent>
                </CardActionArea>
              </Card>
            </a>
          </Grid>

          <Grid item xs={1} sm={4} md={3}>
            <a
              href="https://www.sqlalchemy.org/"
              target="_blank"
              rel="noopener noreferrer"
              style={{ textDecoration: 'none' }}
            >
              <Card sx={{ maxWidth: 345 }} style={styles.card}>
                <CardActionArea style={{ outline: 'none' }}>
                  <CardMedia component="img" image={`${SQLAlchemyLogo}`} style={styles.media} />
                  <CardContent>
                    <Typography gutterBottom variant="h6" component="div">
                      SQLAlchemy
                    </Typography>
                    <Box sx={{ minHeight: '10vh' }}>
                      <Typography variant="body2" color="text.secondary">
                        SQL toolkit and object-relational mapper
                      </Typography>
                    </Box>
                  </CardContent>
                </CardActionArea>
              </Card>
            </a>
          </Grid>

          <Grid item xs={1} sm={4} md={3}>
            <a
              href="https://docs.python.org/3/library/unittest.html"
              target="_blank"
              rel="noopener noreferrer"
              style={{ textDecoration: 'none' }}
            >
              <Card sx={{ maxWidth: 345 }} style={styles.card}>
                <CardActionArea style={{ outline: 'none' }}>
                  <CardMedia component="img" image={`${UnittestLogo}`} style={styles.media} />
                  <CardContent>
                    <Typography gutterBottom variant="h6" component="div">
                      unittest
                    </Typography>
                    <Box sx={{ minHeight: '10vh' }}>
                      <Typography variant="body2" color="text.secondary">
                        Unit testing framework for Python
                      </Typography>
                    </Box>
                  </CardContent>
                </CardActionArea>
              </Card>
            </a>
          </Grid>

          <Grid item xs={1} sm={4} md={3}>
            <a
              href="https://jestjs.io/"
              target="_blank"
              rel="noopener noreferrer"
              style={{ textDecoration: 'none' }}
            >
              <Card sx={{ maxWidth: 345 }} style={styles.card}>
                <CardActionArea style={{ outline: 'none' }}>
                  <CardMedia component="img" image={`${JestLogo}`} style={styles.media} />
                  <CardContent>
                    <Typography gutterBottom variant="h6" component="div">
                      Jest
                    </Typography>
                    <Box sx={{ minHeight: '10vh' }}>
                      <Typography variant="body2" color="text.secondary">
                        Unit testing framework for JavaScript
                      </Typography>
                    </Box>
                  </CardContent>
                </CardActionArea>
              </Card>
            </a>
          </Grid>

          <Grid item xs={1} sm={4} md={3}>
            <a
              href="https://www.selenium.dev/"
              target="_blank"
              rel="noopener noreferrer"
              style={{ textDecoration: 'none' }}
            >
              <Card sx={{ maxWidth: 345 }} style={styles.card}>
                <CardActionArea style={{ outline: 'none' }}>
                  <CardMedia component="img" image={`${SeleniumLogo}`} style={styles.media} />
                  <CardContent>
                    <Typography gutterBottom variant="h6" component="div">
                      Selenium
                    </Typography>
                    <Box sx={{ minHeight: '10vh' }}>
                      <Typography variant="body2" color="text.secondary">
                        Acceptance testing framework for GUIs
                      </Typography>
                    </Box>
                  </CardContent>
                </CardActionArea>
              </Card>
            </a>
          </Grid>

          <Grid item xs={1} sm={4} md={3}>
            <a
              href="https://recharts.org/"
              target="_blank"
              rel="noopener noreferrer"
              style={{ textDecoration: 'none' }}
            >
              <Card sx={{ maxWidth: 345 }} style={styles.card}>
                <CardActionArea style={{ outline: 'none' }}>
                  <CardMedia component="img" image={`${RechartsLogo}`} style={styles.media} />
                  <CardContent>
                    <Typography gutterBottom variant="h6" component="div">
                      Recharts
                    </Typography>
                    <Box sx={{ minHeight: '10vh' }}>
                      <Typography variant="body2" color="text.secondary">
                        Composable charting library
                      </Typography>
                    </Box>
                  </CardContent>
                </CardActionArea>
              </Card>
            </a>
          </Grid>

          <Grid item xs={1} sm={4} md={3}>
            <a
              href="https://plantuml.com/"
              target="_blank"
              rel="noopener noreferrer"
              style={{ textDecoration: 'none' }}
            >
              <Card sx={{ maxWidth: 345 }} style={styles.card}>
                <CardActionArea style={{ outline: 'none' }}>
                  <CardMedia component="img" image={`${PlantUMLLogo}`} style={styles.media} />
                  <CardContent>
                    <Typography gutterBottom variant="h6" component="div">
                      PlantUML
                    </Typography>
                    <Box sx={{ minHeight: '10vh' }}>
                      <Typography variant="body2" color="text.secondary">
                        UML diagrams design tool
                      </Typography>
                    </Box>
                  </CardContent>
                </CardActionArea>
              </Card>
            </a>
          </Grid>

          <Grid item xs={1} sm={4} md={3}>
            <a
              href="https://dbdiagram.io/"
              target="_blank"
              rel="noopener noreferrer"
              style={{ textDecoration: 'none' }}
            >
              <Card sx={{ maxWidth: 345 }} style={styles.card}>
                <CardActionArea style={{ outline: 'none' }}>
                  <CardMedia component="img" image={`${DBDiagramLogo}`} style={styles.media} />
                  <CardContent>
                    <Typography gutterBottom variant="h6" component="div">
                      DBDiagram
                    </Typography>
                    <Box sx={{ minHeight: '10vh' }}>
                      <Typography variant="body2" color="text.secondary">
                        Database relationship diagrams design tool
                      </Typography>
                    </Box>
                  </CardContent>
                </CardActionArea>
              </Card>
            </a>
          </Grid>

          <Grid item xs={1} sm={4} md={3}>
            <a
              href="https://discord.com/"
              target="_blank"
              rel="noopener noreferrer"
              style={{ textDecoration: 'none' }}
            >
              <Card sx={{ maxWidth: 345 }} style={styles.card}>
                <CardActionArea style={{ outline: 'none' }}>
                  <CardMedia component="img" image={`${DiscordLogo}`} style={styles.media} />
                  <CardContent>
                    <Typography gutterBottom variant="h6" component="div">
                      Discord
                    </Typography>
                    <Box sx={{ minHeight: '10vh' }}>
                      <Typography variant="body2" color="text.secondary">
                        Team communication platform
                      </Typography>
                    </Box>
                  </CardContent>
                </CardActionArea>
              </Card>
            </a>
          </Grid>
        </Grid>
      </Container>
    </Box>
  );
};

export default ToolsCards;
