import MemberAndRepoCards from './MemberAndRepoCards';
import ToolsCards from './ToolsCards';
import APICards from './APICards';
import { Box, Typography } from '@mui/material';

const About = () => {
  return (
    <Box>
      <Typography id="aboutPageTitle" gutterBottom variant="h3" component="div" pt={2}>
        About
      </Typography>
      <Typography gutterBottom variant="body1" component="div" pl={4} pr={4} pb={3}>
        Our website makes it easier for people everywhere to learn more about the politicians and
        representatives they are voting for and how they are being influenced. Voting can be a very
        daunting task if students like us and others are not well informed, and we hope to give
        people a different angle to consider when getting a candidate&apos;s full profile. Through
        learning about what committees they are involved in and industries that may be heavily
        influencing them, people everywhere can make more informed decisions regarding who they
        support and want to represent them.
      </Typography>
      <Typography id="teamMembersTitle" gutterBottom variant="h4" component="div">
        Team Members
      </Typography>
      <MemberAndRepoCards />
      <Typography gutterBottom variant="h4" component="div" pt={2}>
        Tools Used
      </Typography>
      <ToolsCards />
      <Typography gutterBottom variant="h4" component="div">
        APIs Used
      </Typography>
      <APICards />
    </Box>
  );
};

export default About;
