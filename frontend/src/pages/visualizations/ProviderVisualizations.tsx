import { Box, Typography } from '@mui/material';
import ProviderCitiesVisualization from './components/ProviderCitiesVisualization';
import ProviderEventsVisualization from './components/ProviderEventsVisualization';
import ProviderHotelsVisualization from './components/ProviderHotelsVisualization';

const ProviderVisualizations = () => {
  return (
    <Box>
      <Typography gutterBottom variant="h5" component="div" pt={2}>
        Provider Visualizations
      </Typography>
      <ProviderEventsVisualization />
      <ProviderCitiesVisualization />
      <ProviderHotelsVisualization />
    </Box>
  );
};

export default ProviderVisualizations;
