import { Box, Typography } from '@mui/material';
import TexasCongresspeopleVisualization from './components/TexasCongresspeopleVisualization';
import IndustriesVisualization from './components/IndustriesVisualization';
import SectorsVisualization from './components/SectorsVisualization';

const OurVisualizations = () => {
  return (
    <Box>
      <Typography gutterBottom variant="h5" component="div" pt={2}>
        Our Visualizations
      </Typography>
      <TexasCongresspeopleVisualization />
      <IndustriesVisualization />
      <SectorsVisualization />
    </Box>
  );
};

export default OurVisualizations;
