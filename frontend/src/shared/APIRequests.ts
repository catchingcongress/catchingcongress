import axios from 'axios';

const URL = 'https://api.catchingcongress.me';
const PROVIDER_URL = 'https://api.goingtoplaces.me';

function deleteEmptyParams(params: any) {
  for (const key of Object.keys(params)) {
    if (params[key] === '') {
      delete params[key];
    }
  }
}

async function getCongresspeople(
  page: number,
  chamber: string,
  political_party: string,
  state: string,
  currently_in_office: string,
  sort: string,
  desc: string,
  q: string
) {
  const params = { page, chamber, political_party, state, currently_in_office, sort, desc, q };
  deleteEmptyParams(params);
  try {
    const response = await axios.get(`${URL}/congresspeople`, {
      headers: {
        'Content-Type': 'application/json',
      },
      params: params,
    });
    return response['data'];
  } catch (error) {
    return console.log(error);
  }
}

async function getCongressperson(cp_id: string) {
  try {
    const response = await axios.get(`${URL}/congresspeople/${cp_id}`, {
      headers: {
        'Content-Type': 'application/json',
      },
    });
    return response['data'];
  } catch (error) {
    return console.log(error);
  }
}

async function getCommittees(
  page: number,
  chamber: string,
  is_primary: string,
  associated_committees: string,
  sort: string,
  desc: string,
  q: string
) {
  const params = { page, chamber, is_primary, associated_committees, sort, desc, q };
  deleteEmptyParams(params);
  try {
    const response = await axios.get(`${URL}/committees`, {
      headers: {
        'Content-Type': 'application/json',
      },
      params: params,
    });
    return response['data'];
  } catch (error) {
    return console.log(error);
  }
}

async function getCommittee(comm_id: string) {
  try {
    const response = await axios.get(`${URL}/committees/${comm_id}`, {
      headers: {
        'Content-Type': 'application/json',
      },
    });
    return response['data'];
  } catch (error) {
    return console.log(error);
  }
}

async function getIndustries(
  page: number,
  sector: string,
  chamber_most_contributed: string,
  party_most_contributed: string,
  sort: string,
  desc: string,
  q: string
) {
  const params = { page, sector, chamber_most_contributed, party_most_contributed, sort, desc, q };
  deleteEmptyParams(params);
  try {
    const response = await axios.get(`${URL}/industries`, {
      headers: {
        'Content-Type': 'application/json',
      },
      params: params,
    });
    return response['data'];
  } catch (error) {
    return console.log(error);
  }
}

async function getIndustry(ind_id: string) {
  try {
    const response = await axios.get(`${URL}/industries/${ind_id}`, {
      headers: {
        'Content-Type': 'application/json',
      },
    });
    return response['data'];
  } catch (error) {
    return console.log(error);
  }
}

async function getNewsArticles(topic: string) {
  await new Promise((resolve) => setTimeout(resolve, 1000));
  const params = {
    q: topic,
    lang: 'en',
    page_size: 3,
  };
  try {
    const response = await axios.get('https://api.newscatcherapi.com/v2/search?', {
      headers: {
        'x-api-key': 'OtvNdHa0t27JGdTES_li6BDNaJzkejExhuA9FJQSVCo',
      },
      params: params,
    });
    return response['data'];
  } catch (error) {
    return console.log(error);
  }
}

async function getProviderEvents() {
  try {
    const response = await axios.get(`${PROVIDER_URL}/events`, {
      headers: {
        'Content-Type': 'application/json',
      },
    });
    return response['data'];
  } catch (error) {
    return console.log(error);
  }
}

async function getProviderCities() {
  try {
    const response = await axios.get(`${PROVIDER_URL}/cities`, {
      headers: {
        'Content-Type': 'application/json',
      },
    });
    return response['data'];
  } catch (error) {
    return console.log(error);
  }
}

async function getProviderHotels() {
  try {
    const response = await axios.get(`${PROVIDER_URL}/hotels`, {
      headers: {
        'Content-Type': 'application/json',
      },
    });
    return response['data'];
  } catch (error) {
    return console.log(error);
  }
}

export {
  getCongresspeople,
  getCongressperson,
  getCommittees,
  getCommittee,
  getIndustries,
  getIndustry,
  getNewsArticles,
  getProviderEvents,
  getProviderCities,
  getProviderHotels,
};
