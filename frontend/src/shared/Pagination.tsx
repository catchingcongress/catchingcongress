import * as React from 'react';
import { TablePagination, Typography } from '@mui/material';

// Borrowed from https://gitlab.com/m3263/musiccity/-/blob/develop/front-end/src/shared_components/CustomPagination.tsx
interface pageData {
  from: number;
  to: number;
  count: number;
  page: number;
}

function defaultLabelDisplayedRows(a: pageData) {
  let total_pages = Math.floor(a.count / (a.to - a.from + 1));
  if (a.count % (a.to - a.from + 1) > 0) {
    total_pages += 1;
  }
  return (
    <Typography variant="body2" component="span">
      {a.from}–{a.to} of {a.count}ㅤ|ㅤPage {a.page + 1} of{' '}
      {a.to === a.count ? a.page + 1 : total_pages}
    </Typography>
  );
}

function Pagination(props: any) {
  const { page, setPage, count } = props;

  const handleChangePage = (event: React.MouseEvent<HTMLButtonElement> | null, newPage: number) => {
    setPage(newPage);
    window.scrollTo(0, 0);
  };

  return (
    <TablePagination
      component="div"
      count={count}
      page={page}
      onPageChange={handleChangePage}
      rowsPerPage={25}
      rowsPerPageOptions={[]}
      labelDisplayedRows={defaultLabelDisplayedRows}
      showFirstButton
      showLastButton
    />
  );
}

export default Pagination;
