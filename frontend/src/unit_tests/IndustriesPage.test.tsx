import { MemoryRouter } from 'react-router-dom';
import { render, screen } from '@testing-library/react';
import Industries from '../pages/industries/IndustriesPage';

describe('Industries Page Testing', () => {
  test('Renders without crashing', () => {
    render(
      <MemoryRouter>
        <Industries />
      </MemoryRouter>
    );
    const element = screen.getAllByText('Industries')[0];
    expect(element).toBeInTheDocument();
  });

  test('Renders searching', async () => {
    render(
      <MemoryRouter>
        <Industries />
      </MemoryRouter>
    );
    const element = screen.getByTestId('search');
    expect(element).toBeInTheDocument();
  });

  test('Renders filtering', async () => {
    render(
      <MemoryRouter>
        <Industries />
      </MemoryRouter>
    );
    const element = screen.getByTestId('filter');
    expect(element).toBeInTheDocument();
  });

  test('Renders sorting', async () => {
    render(
      <MemoryRouter>
        <Industries />
      </MemoryRouter>
    );
    const element = screen.getByTestId('sort');
    expect(element).toBeInTheDocument();
  });
});
