import { render, screen } from '@testing-library/react';
import App from '../App';

describe('App Testing', () => {
  test('Renders without crashing', () => {
    render(<App />);
    const element = screen.getAllByText('Catching Congress')[0];
    expect(element).toBeInTheDocument();
  });
});
