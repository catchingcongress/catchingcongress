# Catching Congress - Frontend

## Dependencies to Run Locally:

- docker
- make (optional)

## How To Do Docker Commands

To build an image with what is currently in your directory, run `make docker-build`. To create a container with this image that you can use to check commands, you can run `make docker-run` and you'll be in a Docker container that has all the dependencies you need. Note that this command mounts the current working directory on the `frontend` directory of the Docker container so that edits made in the container propogate to the directory you are working on locally. If you would like to use the built image (without overwriting anything), just run `docker run --rm -it frontend`.

Without make locally, you can run `docker build -t frontend .` instead of `make docker-build` and `docker run --rm -it -v $PWD:/frontend -w /frontend frontend` instead of `make docker-run`. If using the Windows command prompt, replace `$PWD` with `%cd%`.

## How To Use Code Quality Tools

### Checking Code

Once in the docker container (by running `make docker-run`), simply run `make check-format` to check formatting and `make eslint` to do code analysis. If you'd like to save time, you can make docker do this all in one command (ex. `docker run frontend make check-format`).

Note that you will need to build the docker container if you have made any changes since it was last built. To avoid this, mount the current directory on the `frontend` directory in Docker, by adding the arguments `-v $PWD:/frontend -w /frontend`. If using the Windows command prompt, replace `$PWD` with `%cd%`.

### Formatting Code

Similarly, you can format your files using Prettier by running `make format` in the Docker container or `docker run --rm -v $PWD:/frontend -w /frontend frontend make format` in the local `frontend` directory. If using the Windows command prompt, replace `$PWD` with `%cd%`.
