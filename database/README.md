# Catching Congress - Database

This module contains functions that help set up the database for the Catching Congress website.
It can also be used as a command-line utility, which is likely the easiest way to use it. The code
in this module has also only been tested in Python 3.10. It may work with other versions of Python,
but there are no guarantees, so it is recommended to create and use a virtual environment with
version 3.10 of Python to avoid any issues with version incompatibility (Anaconda and venv are two
tools that may be helpful for Windows and Unix systems, respectively).

The script uses environment variables to avoid exposing sensitive information in source control.

## Environment Variables

`ROOT_DATABASE_USER`

This is the username for a 'root' user of the database. This user must have the ability to create
databases and users, as well as flush privileges. This variable is only used when creating the
database; it is not used otherwise.

`ROOT_DATABASE_PASSWORD`

This is the password for the root user of the password. This variable is only used when creating
the database; it is not used otherwise.

`DATABASE_HOST`

This is the address of the MYSQL database host, used to connect to it.

`DATABASE_USER`

This is the username of user that is setup for this script to use. If creating the database, this
environment variable will be used as the username for the script user that is created. Otherwise,
this environment variable should contain the username of the script user that has already been
created with the appropriate permissions.

`DATABASE_PASSWORD`

This is the password that is to be used for script user.

`DATABASE_NAME`

This is the name of the database. If creating the database, this environment variable is used as
the name of the database. Otherwise, this environment variable should contain the name of the
database that has already been created.

`READONLY_DATABASE_USER`

This is the username of the readonly user that created for use by the web app. If creating the
database, this environment variable will be used as the username for the readonly user that is
created. Otherwise, this variable is not used.

`READONLY_DATABASE_PASSWORD`

This is the password that is to be used for the readonly user that is created. Otherwise, this
variable is not used.

## Examples

Creates a database with two users, a setup user for this script and a readonly user to be used by
the web app. It also fills in the tables with data from the JSON files in the directory.

    $ python db_setup.py -ct

Using an already created database, drop the existing tables and create and fill them in.
    
    $ python db_setup.py -dt

Using an already created database, drop the existing tables and create them without filling them in
again.
    
    $ python db_setup.py -dtn

Using an already created database, drop the existing tables and create them and fill them in with a
custom file location for the congresspeople JSON file.
    
    $ python db_setup.py -dt --congresspeople custom.json

### More Information

For more information, run:

    $ python db_setup.py --help