import json
from flask import Flask
from flask_cors import CORS
from flask_marshmallow import Marshmallow
from marshmallow import fields, pre_dump, post_dump
from database import init_database

# Texas Votes https://gitlab.com/forbesye/fitsbits/-/blob/master/back-end/models.py
# used to follow the structure of the file

app = Flask(__name__)
CORS(app)
db = init_database(app)
ma = Marshmallow(app)

# An Association table between Congresspeople and Committees they are associated with. Many to Many.
class CommitteeMembership(db.Model):
    __tablename__ = "committee_membership"
    cp_id = db.Column(
        db.String,
        db.ForeignKey("congresspeople.cp_id"),
        primary_key=True,
        nullable=False,
    )
    comm_id = db.Column(
        db.String, db.ForeignKey("committees.comm_id"), primary_key=True, nullable=False
    )
    currently_a_member = db.Column(db.Boolean, nullable=False)
    # committee = db.relationship('Committee', back_populates="members")
    # congressperson = db.relationship('Congressperson', back_populates="committees")


# An Association table between a committee and its Parent or Sub committees. One to Many.
class CommitteeRelations(db.Model):
    __tablename__ = "committee_relations"
    parent_committee_id = db.Column(
        db.String, db.ForeignKey("committees.comm_id"), primary_key=True, nullable=False
    )
    subcommittee_id = db.Column(
        db.String, db.ForeignKey("committees.comm_id"), primary_key=True, nullable=False
    )


# An Association table between a congressperson and the industries which contributed to it.
class CongresspersonContributions(db.Model):
    __tablename__ = "congressperson_contributions"
    cp_id = db.Column(
        db.String,
        db.ForeignKey("congresspeople.cp_id"),
        primary_key=True,
        nullable=False,
    )
    ind_id = db.Column(
        db.String, db.ForeignKey("industries.ind_id"), primary_key=True, nullable=False
    )
    amount = db.Column(db.Integer, nullable=False)


class Congressperson(db.Model):
    __tablename__ = "congresspeople"
    # ids
    cp_id = db.Column(db.String, primary_key=True, nullable=False)
    crp_id = db.Column(db.String, nullable=False)
    fec_candidate_id = db.Column(db.String, nullable=False)
    google_entity_id = db.Column(db.String, nullable=False)
    # Variables
    name = db.Column(db.String, nullable=False)
    description = db.Column(db.Text, nullable=False)
    political_party = db.Column(db.Enum("D", "R", "I"), nullable=False)
    state = db.Column(db.String, nullable=False)
    gender = db.Column(db.String, nullable=False)
    date_of_birth = db.Column(db.Date, nullable=False)
    chamber = db.Column(db.Enum("house", "senate"), nullable=False)
    title = db.Column(db.String, nullable=False)
    currently_in_office = db.Column(db.Boolean, nullable=False, default=False)
    percentage_votes_missed = db.Column(db.Float)
    percentage_votes_with_political_party = db.Column(db.Float)
    image_url = db.Column(
        db.String,
        nullable=False,
        default="https://smhlancers.org/wp-content/uploads/2016/06/profile-placeholder.png",
    )
    top_contributors = db.Column(db.JSON, nullable=False)

    # Extra embedded media?
    url = db.Column(db.String, nullable=False)  # website
    twitter_account = db.Column(db.String, nullable=False)
    youtube_account = db.Column(db.String)
    facebook_account = db.Column(db.String)
    phone = db.Column(db.String, nullable=False)
    office_address = db.Column(db.String, nullable=False)

    # Relationships
    # committees = db.relationship("CommitteeMembership", back_populates="committee")

    def __repr__(self):
        return "{self.name}, {self.party}, a congressperson in the {self.chamber}."


class Committee(db.Model):
    __tablename__ = "committees"
    # ids
    comm_id = db.Column(db.String, primary_key=True, nullable=False)

    # Variables
    name = db.Column(db.String, nullable=False)
    is_primary = db.Column(db.Boolean, nullable=False)
    chamber = db.Column(db.Enum("house", "senate", "joint"), nullable=False)
    chair_id = db.Column(db.String)
    ranking_member_id = db.Column(db.String)
    url = db.Column(db.String, nullable=True)  # website

    # relationships
    # members = db.relationship("CommitteeMembership", back_populates="congressperson")

    def __repr__(self):
        return "{self.name}, a {self.comm_status} committee in the {self.chamber}."


class Industry(db.Model):
    __tablename__ = "industries"
    ind_id = db.Column(db.String, primary_key=True, nullable=False)
    # Variables
    name = db.Column(db.String, nullable=False)
    sector = db.Column(db.String, nullable=False)
    categories = db.Column(db.JSON, nullable=False)
    total_contributed = db.Column(db.Integer)
    party_most_contributed = db.Column(db.Enum("R", "D", "I"), nullable=False)
    chamber_most_contributed = db.Column(db.Enum("house", "senate"), nullable=True)
    state_most_contributed = db.Column(db.String)
    top_contributors = db.Column(db.JSON)


class BaseSchema(ma.Schema):
    RELATIONSHIPS = []
    SINGLE_RELATIONSHIPS = []
    JSON_DUMPS = []

    @pre_dump
    def cleanup_relationship_values(self, data, **kwargs):
        # pylint: disable=unused-argument
        ret_dict = {}
        for key, value in data.items():
            if key in self.JSON_DUMPS:
                value = json.loads(value) if value is not None else None
            processed_as_relationship = False
            if value is not None:
                for rel in self.RELATIONSHIPS:
                    if rel in key and not processed_as_relationship:
                        obj_key = key[len(rel) + 1 :]
                        value_list = (
                            value.split(":")
                            if rel not in self.SINGLE_RELATIONSHIPS
                            else [value]
                        )
                        if rel not in ret_dict:
                            ret_dict[rel] = [{} for _ in range(len(value_list))]
                        for value_item, obj in zip(value_list, ret_dict[rel]):
                            obj[obj_key] = value_item
                        processed_as_relationship = True
            if not processed_as_relationship:
                ret_dict[key] = value

        for rel in self.SINGLE_RELATIONSHIPS:
            if rel in ret_dict:
                assert len(ret_dict[rel]) == 1
                ret_dict[rel] = ret_dict[rel][0]

        return ret_dict

    @post_dump
    def remove_none_values(self, data, **kwargs):
        # pylint: disable=unused-argument,no-self-use
        return {key: value for key, value in data.items() if value is not None}


class MajorContributingIndustriesSchema(BaseSchema):
    ind_id = fields.Str(required=True)
    name = fields.Str(required=True)
    amount = fields.Integer(required=True)


class CommitteeMembershipsSchema(BaseSchema):
    comm_id = fields.Str(required=True)
    name = fields.Str(required=True)
    currently_a_member = fields.Bool(required=True)


class ContributorSchema(BaseSchema):
    name = fields.Str(required=True)
    amount = fields.Integer(required=True)


class TopCandidatesContributedSchema(BaseSchema):
    cp_id = fields.Str(required=True)
    name = fields.Str(required=True)
    amount = fields.Integer(required=True)


class TopCommitteesContributedSchema(BaseSchema):
    comm_id = fields.Str(required=True)
    name = fields.Str(required=True)
    amount = fields.Integer(required=True)


class CommitteeMemberSchema(BaseSchema):
    cp_id = fields.Str(required=True)
    name = fields.Str(required=True)
    state = fields.Str(required=True)
    political_party = fields.Str(required=True)
    currently_a_member = fields.Bool(required=True)


class AssociatedCommitteeSchema(BaseSchema):
    comm_id = fields.Str(required=True)
    name = fields.Str(required=True)
    is_primary = fields.Bool(required=True)


class CongresspersonSchema(BaseSchema):
    RELATIONSHIPS = ["major_contributing_industries", "committees"]

    # ids
    cp_id = fields.Str(required=True)
    crp_id = fields.Str(required=True)
    fec_candidate_id = fields.Str(required=True)
    google_entity_id = fields.Str(required=True)

    # Variables
    name = fields.Str(required=True)
    description = fields.Str(required=True)
    political_party = fields.Str(required=True)
    state = fields.Str(required=True)
    gender = fields.Str(required=True)
    date_of_birth = fields.Date(required=True)
    chamber = fields.Str(required=True)
    title = fields.Str(required=True)
    currently_in_office = fields.Bool(required=True)
    percent_votes_missed = fields.Float(required=False)
    percent_votes_with_party = fields.Float(required=False)

    # rich media
    image_url = fields.Str(required=True, attribute="image_url")
    url = fields.Str(required=True)
    twitter_account = fields.Str(required=True)
    facebook_account = fields.Str(required=False)
    youtube_account = fields.Str(required=False)
    phone = fields.Str(required=True, attribute="phone")
    office_address = fields.Str(required=True)

    # JSON
    major_contributing_industries = fields.List(
        fields.Nested(MajorContributingIndustriesSchema), required=False
    )
    committees = fields.List(fields.Nested(CommitteeMembershipsSchema), required=False)
    top_contributors = fields.List(fields.Nested(ContributorSchema), required=False)


class CommitteeSchema(BaseSchema):
    RELATIONSHIPS = [
        "major_contributing_industries",
        "members",
        "chair",
        "ranking_member",
        "associated_committees",
    ]
    SINGLE_RELATIONSHIPS = ["chair", "ranking_member"]
    JSON_DUMPS = ["sectors_recieved_money_from"]

    # ids
    comm_id = fields.Str(required=True)

    # Variables
    name = fields.Str(required=True)
    is_primary = fields.Bool(required=True)
    chamber = fields.Str(required=True)
    num_subcommittees = fields.Integer(required=True)

    # media
    url = fields.Str(required=False)

    # JSON
    major_contributing_industries = fields.List(
        fields.Nested(MajorContributingIndustriesSchema), required=False
    )
    members = fields.List(fields.Nested(CommitteeMemberSchema), required=False)
    chair = fields.Nested(CommitteeMemberSchema, required=False)
    ranking_member = fields.Nested(CommitteeMemberSchema, required=False)
    sectors_recieved_money_from = fields.List(fields.Str(), required=False)
    associated_committees = fields.List(
        fields.Nested(AssociatedCommitteeSchema), required=False
    )


class IndustrySchema(BaseSchema):
    RELATIONSHIPS = ["top_candidates_contributed", "top_committees_contributed"]

    # ids
    ind_id = fields.Str(required=True)

    # Variables
    name = fields.Str(required=True)
    sector = fields.Str(required=True)
    total_contributed = fields.Integer(required=False)
    party_most_contributed = fields.Str(required=False)
    chamber_most_contributed = fields.Str(required=False)
    state_most_contributed = fields.Str(required=False)
    top_contributors = fields.Str(required=False)

    # JSON
    categories = fields.List(fields.Str(required=True), required=True)
    top_candidates_contributed = fields.List(
        fields.Nested(TopCandidatesContributedSchema), required=False
    )
    top_committees_contributed = fields.List(
        fields.Nested(TopCommitteesContributedSchema), required=False
    )
    top_contributors = fields.List(fields.Nested(ContributorSchema), required=False)
