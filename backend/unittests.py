# these are the unit tests we are using to test our api
from unittest import TestCase, main
from app import app


class UnitTests(TestCase):
    def test_congresspeople(self):
        test_client = app.test_client()
        req = test_client.get("/congresspeople")
        self.assertEqual(req.status_code, 200)

    def test_congresspeople_page_3(self):
        test_client = app.test_client()
        req = test_client.get("/congresspeople?page=3")
        self.assertEqual(req.status_code, 200)

    # this is with the CP ID
    def test_congresspeople_id(self):
        test_client = app.test_client()
        req = test_client.get("/congresspeople/B001230")
        self.assertEqual(req.status_code, 200)

    def test_committees(self):
        test_client = app.test_client()
        req = test_client.get("/committees")
        self.assertEqual(req.status_code, 200)

    def test_committees_id(self):
        test_client = app.test_client()
        req = test_client.get("/committees/HSHA")
        self.assertEqual(req.status_code, 200)

    def test_committees_page(self):
        test_client = app.test_client()
        req = test_client.get("/committees?page=1")
        self.assertEqual(req.status_code, 200)

    def test_industries(self):
        test_client = app.test_client()
        req = test_client.get("/industries")
        self.assertEqual(req.status_code, 200)

    def test_industries_id(self):
        test_client = app.test_client()
        req = test_client.get("/industries/A11")
        self.assertEqual(req.status_code, 200)

    def test_industries_page(self):
        test_client = app.test_client()
        req = test_client.get("/industries?page=1")
        self.assertEqual(req.status_code, 200)

    def test_congresspeople_filter(self):
        test_client = app.test_client()
        req = test_client.get("/congresspeople?political_party=D")
        self.assertEqual(req.status_code, 200)

    def test_congresspeople_multiple_filters(self):
        test_client = app.test_client()
        req = test_client.get("/congresspeople?political_party=D&chamber=house&page=3")
        self.assertEqual(req.status_code, 200)

    def test_committees_filter(self):
        test_client = app.test_client()
        req = test_client.get("/committees?is_primary=1")
        self.assertEqual(req.status_code, 200)

    def test_committees_multiple_filters(self):
        test_client = app.test_client()
        req = test_client.get("/committees?is_primary=1&chamber=senate&page=1")
        self.assertEqual(req.status_code, 200)

    def test_industries_filter(self):
        test_client = app.test_client()
        req = test_client.get("/industries?sector=Agribusiness")
        self.assertEqual(req.status_code, 200)

    def test_industries_multiple_filters(self):
        test_client = app.test_client()
        req = test_client.get(
            "/industries?sector=Transportation&chamber_most_contributed=house&page=1"
        )
        self.assertEqual(req.status_code, 200)


if __name__ == "__main__":
    main()
